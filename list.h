
typedef struct CircularSinglyLinkedList{
	char info;
	int index;
	bool snBase;
	struct CircularSinglyLinkedList *prox;
}CircularSinglyLinkedList;


CircularSinglyLinkedList* createCircularSinglyLinkedList();
void destroyCircularSinglyLinkedList(CircularSinglyLinkedList** list_);
void insertInCircularSinglyLinkedList(CircularSinglyLinkedList* list, char value, int index);
char removeFromCircularSinglyLinkedList(CircularSinglyLinkedList* list, int index);
void setValueInCircularSinglyLinkedList(CircularSinglyLinkedList* list, char value, int index);
