#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "list.h"


// Criar lista:	Cria a lista e retorna o apontador � mesma.
CircularSinglyLinkedList* createCircularSinglyLinkedList(){
	CircularSinglyLinkedList *inicio = (CircularSinglyLinkedList *)malloc(sizeof(CircularSinglyLinkedList));
	inicio->info= -1;
	inicio->prox = inicio;

	return inicio;
}

// Destruir lista: Recebe um apontador duplo � lista
//e libera a mem�ria atribu�da a todos seus dados.
void destroyCircularSinglyLinkedList(CircularSinglyLinkedList** list_){
	CircularSinglyLinkedList **aux1 = list_;
	CircularSinglyLinkedList **aux2 = list_;

	while ((*aux1)->prox != list_){
		*aux2 = (*aux1)->prox;
		free(*aux1);
		aux1 = aux2;
	}

}

//Insertar em lista: Recede a lista, o valor a ser inserto
//e a posi��o onde ele deve ficar.
void insertInCircularSinglyLinkedList(CircularSinglyLinkedList* list, char value, int index){
	int i = 0;
	//Verifica se a posi��o � valida e se a lista est� vazia
	if (index < 0 || list == NULL){
		return;
	}

	CircularSinglyLinkedList *aux = createCircularSinglyLinkedList();
	aux = list; //auxiliar aponta para o come�o da lista
	//list=list->prox;
	bool flag = true;

	while (flag){
		if (list->prox->info == -1){
			CircularSinglyLinkedList *aux1 = createCircularSinglyLinkedList();

			aux1->info = value;
			aux1->index = index;
			aux1->snBase = true;
			aux1->prox = list->prox;
			list->prox = aux1;
			i++;
			flag = !flag;
		}
		if (value == '\n'){
			break;
		}
		else{
			list = list->prox;
			i++;
		}
	}

	list = aux;
}


void printLista(CircularSinglyLinkedList *l){
	CircularSinglyLinkedList *aux;
	aux = l->prox;
	if (l == NULL)
		puts("Lista vazia");
	else{
		do{
			printf("%c", aux->info);
			aux = aux->prox;
		} while (aux != l);
	}

}
// Apagar elemento:	Recebe a lista e a posi��o do elemento
// a ser apagado. Libera a mem�ria atribu�da ao n� e retorna
//o valor do n� que foi exclu�do.
char removeFromCircularSinglyLinkedList(CircularSinglyLinkedList* list, int index){
	char c;
	//Verifica se a posi��o � valida e se a lista est� vazia
	if (index < 0 || list == NULL){
		return 0;
	}

	CircularSinglyLinkedList *aux = createCircularSinglyLinkedList();
	*aux = *list; //auxiliar aponta para o come�o da lista
	while (aux->prox != list){
		if (aux->prox->index == index){
			c = aux->prox->info;
			aux->prox = aux->prox->prox;
			free(aux->prox);
			return c;
		}
		else{
			aux = aux->prox;
		}
	}
}

// Atualizar elemento:	Recebe a lista,
//o novo valor e a posi��o do n� que deve ser atualizado.
void setValueInCircularSinglyLinkedList(CircularSinglyLinkedList* list, char value, int index){
	char c;
	//Verifica se a posi��o � valida e se a lista est� vazia
	if (index < 0 || list == NULL){
		return 0;
	}

	CircularSinglyLinkedList *aux = createCircularSinglyLinkedList();
	*aux = *list; //auxiliar aponta para o come�o da lista
	while (aux->prox != list){
		if (aux->index == index){

		}
	}
}
