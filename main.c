#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "list.h"

int *findSubsequence(CircularSinglyLinkedList* list, CircularSinglyLinkedList* busca){
	CircularSinglyLinkedList *aux1 = list->prox;
	CircularSinglyLinkedList *aux2 = busca->prox;
	CircularSinglyLinkedList *aux3;
	int n = 1, *indexes, *vet;

	for(aux3 = list->prox; aux3->info != -1; aux3=aux3->prox);
        n++;

    indexes = malloc(n*sizeof(int));
    n = 0;

	do{
		aux3 = aux1;

		do{
			//printf("%c ", aux1->info);
			if (aux3->info == -1)
				aux3 = aux3->prox;
			if (aux2->info == aux3->info){
				aux2 = aux2->prox;
				aux3 = aux3->prox;
				if (aux2 == busca)
					indexes[n++] = aux1->index;
			}
			else
				aux2 = busca;
		} while (aux2 != busca);

		aux2 = aux2->prox;
		aux1 = aux1->prox;
	} while (aux1 != list);

	return indexes;
}


int *corte(CircularSinglyLinkedList* list, CircularSinglyLinkedList* restricao, int index, bool snCorte,
	CircularSinglyLinkedList *busca){
	char rna[511];
	int i, n, rnaReverseIndex, listIndex, j, *indexes, *vet;
	CircularSinglyLinkedList* aux = list->prox;
	CircularSinglyLinkedList* rnaReverse = createCircularSinglyLinkedList();
	CircularSinglyLinkedList* aux1, *aux2;

	n = i = 0;

	while(aux->info != -1){
		if(aux->info == 'a')
			rna[n] = 't';
		else if(aux->info == 't')
			rna[n] = 'a';
		else if(aux->info == 'c')
			rna[n] = 'g';
		else if(aux->info == 'g')
			rna[n] = 'c';

		n++;
		aux = aux->prox;
	}

	for(i=n-1; i>=0; i--){
		insertInCircularSinglyLinkedList(rnaReverse, rna[i], i);
	}

	vet = findSubsequence(rnaReverse,restricao);
	rnaReverseIndex = (vet[0] - index) % n;
	vet = findSubsequence(list, restricao);
	listIndex = (vet[0] + index) % n;

	/***************************************************************/
	for (aux = list->prox; aux->info != -1; aux = aux->prox){
		if (rnaReverseIndex > listIndex &&
			(aux->index <= rnaReverseIndex && aux->index >= listIndex))
		{
			aux->snBase = false;
		}
		else if (rnaReverseIndex+1 < listIndex &&
			!(aux->index <= rnaReverseIndex && aux->index >= listIndex)){
			aux->snBase = true;
		}
	}

	for (aux = rnaReverse->prox; aux->info != -1; aux = aux->prox){
		if (rnaReverseIndex > listIndex &&
			(aux->index <= rnaReverseIndex && aux->index >= listIndex))
		{
			aux->snBase = false;
		}
		else if (rnaReverseIndex+1 < listIndex &&
			!(aux->index <= rnaReverseIndex && aux->index >= listIndex)){
			aux->snBase = true;
		}
	}
	/***************************************************************/

	if (snCorte){
		for (aux = list->prox; aux->info != -1; aux = aux->prox){
			if (aux->prox->index == listIndex - 1){
				aux1 = aux->prox->prox->prox;

				for (aux2 = rnaReverse->prox; aux2->info != -1; aux2 = aux2->prox)
					if (aux2->index == aux->prox->index || aux2->index == aux->prox->prox->index)
						aux2->snBase = false;

				free(aux->prox->prox);
				free(aux->prox);

				aux->prox = aux1;
				listIndex--;

				break;
			}
		}

		for (aux = rnaReverse->prox; aux->info != -1; aux = aux->prox){
			if (aux->prox->index == rnaReverseIndex + 1){
				aux1 = aux->prox->prox->prox;

				for (aux2 = list->prox; aux2->info != -1; aux2 = aux2->prox)
					if (aux2->index == aux->prox->index || aux2->index == aux->prox->prox->index)
						aux2->snBase = false;

				free(aux->prox->prox);
				free(aux->prox);

				aux->prox = aux1;
				rnaReverseIndex++;

				break;
			}
		}
	}


	for (aux = list->prox; aux->info != -1; aux = aux->prox){
		if (aux->index == listIndex - 1){
			aux1 = aux;
		}
		else if (aux->prox->info == -1){
			aux->prox = list->prox;
			list->prox = aux1->prox;
			aux1->prox = list;

			break;
		}
	}

	for (aux = rnaReverse->prox; aux->info != -1; aux = aux->prox){
		if (aux->index == rnaReverseIndex + 1){
			aux1 = aux;
		}
		else if (aux->prox->info == -1){

			aux->prox = rnaReverse->prox;
			rnaReverse->prox = aux1->prox;
			aux1->prox = rnaReverse;

		}
	}



	/***********************************************************/


	for (aux = list->prox, i = 0; aux->info != -1; aux = aux->prox, i++)
		aux->index = i;

	for (aux = list->prox, i = 0; aux->info != -1; aux = aux->prox, i++){
		if (aux->snBase){
			for (aux = rnaReverse->prox, j=0; aux->info != -1; aux = aux->prox, j++)
				aux->index = n+i-j-1;
			break;
		}
	}


	indexes = malloc(n*sizeof(int));
	i = 0;

	vet = findSubsequence(list, busca);

	for (j=0; vet[j] != -1; j++)
		for (aux1 = list->prox; aux1->info != -1; aux1 = aux1->prox)
			if (aux1->index == vet[j] && aux1->snBase)
				indexes[i++] = vet[j];

	vet = findSubsequence(rnaReverse, busca);

	for (j = 0, aux1 = busca->prox; aux1->info != -1; j++, aux1 = aux1->prox);

	for (j=0; vet[j] != -1; j++)
		for (aux1 = rnaReverse->prox; aux1->info != -1; aux1 = aux1->prox)
			if (aux1->index == vet[j] && aux1->snBase)
				indexes[i++] = vet[j] - j + 1;

	indexes[i] = -1;


	return indexes;
}

int main(){
	char value, str[511];
	int i = 1, indiceCorte, *indexes;
	bool snCorte;
	CircularSinglyLinkedList *list = createCircularSinglyLinkedList();
	CircularSinglyLinkedList *busca = createCircularSinglyLinkedList();
	CircularSinglyLinkedList *restricao = createCircularSinglyLinkedList();


	//Le a lista e insere o valor em list
	for (i = 0, value = 0; value != '\n'; i++){
		scanf("%c", &value);
		insertInCircularSinglyLinkedList(list, value, i);
	}

	//str = malloc((i + 8)*sizeof(char));
	scanf("%s %d %d", str, &indiceCorte, &snCorte);

	for (i = 0; str[i] != '\0'; i++)
		insertInCircularSinglyLinkedList(restricao, str[i], i);

	//Le a lista de busca
	getchar(); // Le o '\n'
	for (i = 0, value = 'a'; value != '\n'; i++){
		scanf("%c", &value);
		insertInCircularSinglyLinkedList(busca, value, i);
	}



	/*do{
		printf("%d ", resBusca->index);
		resBusca = resBusca->prox;
	} while (resBusca->info != -1);
	printf("\n");*/

	/*printLista(list);
	printf("\n");
	printLista(restricao);
	printf("\n");
	printLista(busca);*/

	indexes = corte(list, restricao, indiceCorte, snCorte, busca);

	if (indexes[0] == -1)
		printf("NE");
	for (i = 0; indexes[i] != -1; i++)
		printf("%d ", indexes[i]);

	free(str);
	printf("\n");

	return 0;
}
